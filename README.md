# Pandas based vectorized backtest engine

- Currently based on `fx-beta`
- Supports OHLC data (tick level ingestion :wip:)

## Installation and Running

### Installation

- Install the `ta-lib` [dependency](http://ta-lib.org/hdr_dw.html)
- Install Python deps from Pipfile

```bash
pipenv install --skip-lock
```

### Running

- Backtest:

```
./back.py -d dukas_ohlc.h5 -c configs/ -r results
```

```
usage: back.py [-h] [-d DATA] [-r RESULTS] [-c CONFIGS] [-t THREADS]
               [-p PROCESS]

optional arguments:
  -h, --help            show this help message and exit
  -d DATA, --data DATA  Data h5 file with ohlc/pair data
  -r RESULTS, --results RESULTS
                        Results directory
  -c CONFIGS, --configs CONFIGS
                        Configuration directory
  -t THREADS, --threads THREADS
                        Threads per process
  -p PROCESS, --process PROCESS
                        Number of cores to run
```

- Gradient:

```
usage: gradient.py [-h] [--ranges RANGES] [--process PROCESS] [--model MODEL]
                   [--threshold THRESHOLD] [--checkpoint CHECKPOINT]
```

```
optional arguments:
  -h, --help            show this help message and exit
  --ranges RANGES, -r RANGES
                        Specify the ranges for the parameters
  --process PROCESS, -p PROCESS
                        Number of batches to run at a time
  --model MODEL, -m MODEL
                        Model to use for the variation predictions
  --threshold THRESHOLD, -t THRESHOLD
                        Minumum threshold to be logged to file
  --checkpoint CHECKPOINT, -c CHECKPOINT
```
