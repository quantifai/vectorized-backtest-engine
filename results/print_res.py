#!/usr/bin/env python3
# coding: utf-8
import argparse
import os

import pandas as pd

def collect_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=str,
            default="small_new", help="Input file")
    args = parser.parse_args()
    return args


def main():
    args = collect_args()
    data = [_ for _ in open(args.input).read().splitlines() if _ != '0']
    df = pd.DataFrame([data[i:i + 6] for i in range(0, len(data), 6)], columns=['profit', 'loss', 'total', 'win_rate', 'epic', 'variation'])
    df.set_index("variation", inplace=True)
    df.profit = pd.to_numeric(df.profit)
    df.loss = pd.to_numeric(df.loss)
    df.total = pd.to_numeric(df.total)
    df.win_rate = pd.to_numeric(df.win_rate)
    grouped = df.groupby('variation').mean()
    grouped.to_csv('grouping.csv')
    print(grouped, df.groupby('epic').mean(), sep="\n")


if __name__ == "__main__":
    main()

