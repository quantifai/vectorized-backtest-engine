#!/usr/bin/env python3
# coding: utf-8
import argparse
from collections import deque
from datetime import datetime
import glob
from math import ceil
from multiprocessing import Process
import os
import pickle
import sys

import pandas as pd
import talib as ta
import yaml


epics = {
    "EURUSD": {'currencyCode':"USD/USD", 'pip': 0.0001, 'perlot': 100000, "accountID": 'QuantifAI_1' , 'lotexponent': 1 },
    "USDCAD": {'currencyCode':"USD/CAD", 'pip': 0.0001, 'perlot': 100000, "accountID": 'QuantifAI_2' , 'lotexponent': 1 },
    "EURCHF": {'currencyCode':"USD/CHF", 'pip': 0.0001, 'perlot': 100000, "accountID": 'QuantifAI_3', 'lotexponent': 1 },
    "CADCHF": {'currencyCode':"USD/CAD", 'pip': 0.0001, 'perlot': 100000, "accountID": 'QuantifAI_4', 'lotexponent': 1 },
    "EURJPY": {'currencyCode':"USD/JPY", 'pip': 0.0100, 'perlot': 100000, "accountID": 'QuantifAI_5' , 'lotexponent': 1 },
    "GBPUSD": {'currencyCode':"USD/USD", 'pip': 0.0001, 'perlot': 100000, "accountID": 'QuantifAI_6' , 'lotexponent': 1 },
    "EURGBP": {'currencyCode':"GBP/USD", 'pip': 0.0001, 'perlot': 100000, "accountID": 'QuantifAI_7' , 'lotexponent': -1},
    "USDCHF": {'currencyCode':"USD/CHF", 'pip': 0.0001, 'perlot': 100000, "accountID": 'QuantifAI_8' , 'lotexponent': 1 },
    "GBPJPY": {'currencyCode':"USD/JPY", 'pip': 0.0100, 'perlot': 100000, "accountID": 'QuantifAI_9' , 'lotexponent': 1 },
    "AUDJPY": {'currencyCode':"USD/JPY", 'pip': 0.0100, 'perlot': 100000, "accountID": 'QuantifAI_10', 'lotexponent': 1 },
}


def collect_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data', type=str,
                        help="Data h5 file with ohlc/pair data")
    parser.add_argument('-r', '--results', type=str, default='results',
                        help="Results directory")
    parser.add_argument('-c', '--configs', type=str, default='configs',
                        help="Configuration directory")
    parser.add_argument('-t', '--threads', type=int, default=2,
                        help="Threads per process")
    parser.add_argument('-p', '--process', type=int, default=8,
                        help="Number of cores to run")
    args = parser.parse_args()
    return args


def MACD(close, fastperiod=None, slowperiod=None, signalperiod=None):
    """Calculate MACD values manually

    :param close: pd.Series -> Close values vector
    :param fastperiod: int -> ema-fast
    :param slowperiod: int -> ema-slow
    :param signalperiod: int -> macd ema-signalperiod
    :return macd_histo: -> pd.Series -> Histo calues
    """
    ema_fast = pd.Series.ewm(close, span=fastperiod).mean()
    ema_slow = pd.Series.ewm(close, span=slowperiod).mean()
    # ema_fast = ta.EMA(close, fastperiod)
    # ema_slow = ta.EMA(close, slowperiod)
    macd = ema_fast - ema_slow
    macd_signal = pd.Series.ewm(macd, span=signalperiod).mean()
    # macd_signal = ta.EMA(macd, signalperiod)
    macd_histo = macd - macd_signal
    del ema_fast, ema_slow, macd, macd_signal
    return macd_histo


def backtest(df, pair, config, args, variation):
    """Backtest sequentially"""
    try:
        pip = epics[pair]['pip']
    except KeyError:
        pip = 0.0100 if 'JPY' in pair else 0.0001
    ohlc = ['Open', 'High', 'Low', 'Close']
    df_ = df[ohlc]
    df[ohlc] = round(df[ohlc] / pip) * pip
    df['adx'] = ta.ADX(df.High, df.Low, df.Close, config['adx'])
    # df['macd'] = MACD(df.Close, fastperiod=config['fastperiod'],
    #                   slowperiod=config['slowperiod'],
    #                   signalperiod=config['signalperiod'])
    _, _, df['macd'] = ta.MACD(df.Close, fastperiod=config['fastperiod'],
                      slowperiod=config['slowperiod'],
                      signalperiod=config['signalperiod'])
    stoch_invalid = any([config[f'stoch_{x}'] == 0 for x in ['fast', 'slow', 'long', 'short']])
    df['stoch'] = df.High
    if not stoch_invalid:
        df['stoch'], _ = ta.STOCH(df.High, df.Low, df.Close, fastk_period=config['stoch_fast'], slowk_period=config['stoch_slow'])


    df[['uHigh', 'uLow']] = df_[['High', 'Low']]
    adx_ = deque(maxlen=3)
    macd_ = deque(maxlen=2 + config['histo_check'] + config['macd_shift'])
    stoch_ = deque(maxlen=1)
    is_active = False
    is_signal = df.index[0]
    recalc = True
    entries = []
    signals = deque(maxlen=1)
    low = deque(maxlen=1)
    high = deque(maxlen=1)
    exits = []
    profits = 0
    losses = 0
    macd_delay = 1 if config['histo_check'] == 0 else config['histo_check']
    for timestamp, row in df.dropna(subset=['macd', 'adx']).iterrows():
        # sys.stdout.write(f"{len(entries)}, {len(exits)}\r")
        if is_active is False and len(adx_) >= 3:
            if len(macd_) >= 1 + config['histo_check'] + config['macd_shift']:
                adx_bool = adx_[-1] > adx_[-2] > adx_[-3]
                signal = False
                flag = False
                if macd_[-macd_delay] < 0 and adx_bool and (stoch_[-1] < config['stoch_long'] or stoch_invalid):  # long condition
                    signal = "LONG"
                    entry = low[-1] - config['shift'] * pip
                    flag = True
                elif macd_[-macd_delay] > 0 and (adx_bool if config['adx_shift'] == 0 else adx_[-1] < adx_[-2] < adx_[-3]) and (stoch_[-1] > config['stoch_short'] or stoch_invalid):  # short condition
                    signal = "SHORT"
                    entry = high[-1] + config['shift'] * pip
                    flag = True

                for shift_ in range(config['macd_shift']):
                    if signal == "LONG":
                        flag = flag and macd_[-macd_delay - shift_] > macd_[-macd_delay - shift_ - 1]
                    elif signal == "SHORT":
                        flag = flag and macd_[-macd_delay - shift_] < macd_[-macd_delay - shift_ - 1]

                if flag:
                    is_signal = timestamp
                    signals.append({
                        'type': signal,
                        'entry': entry,
                        'timestamp': timestamp,
                    })
                    # print("[SIGNAL]", signals[-1])


        if is_signal and not is_active:
            if len(signals) != 0 and timestamp.minute - is_signal.minute <= config['timeout']:
                signal = signals[-1]
                if row['uLow'] <= signal['entry'] <= row['uHigh']:  # valid entry
                    is_active = True
                    is_signal = 0
                    signals.pop()
                    if signal['type'] == "LONG":
                        stoploss = signal['entry'] - config['sl'] * pip
                        target = signal['entry'] + config['tp'] * pip
                    elif signal['type'] == "SHORT":
                        stoploss = signal['entry'] + config['sl'] * pip
                        target = signal['entry'] - config['tp'] * pip
                    entries.append({
                        'type': signal['type'],
                        'entry': entry,
                        'timestamp': timestamp,
                        'sl': stoploss,
                        'tp': target
                    })

            elif len(signals) != 0:  # reset signal
                # print("[RESET]", signals[-1])
                is_signal = datetime.now()
                signals.pop()

        if is_active:  # check for exit
            entry = entries[-1]
            reset = False
            type = None
            if row['uLow'] <= entry['tp'] <= row['uHigh']: # profit
                reset = True
                type = "PROFIT"
                profits += 1
            elif row['uLow'] <= entry['sl'] <= row['uHigh']: # loss
                reset = True
                type = "LOSS"
                losses += 1

            if reset:
                is_active = False
                exits.append({
                    'timestamp': timestamp,
                    'exit': entry['tp'] if type == "PROFIT" else entry['sl'],
                    'position': entry['type'],
                    'type': type
                })
                if recalc:
                    recalc = False
                    continue
                else:
                    recalc = True
                # print("[TRADE]", entries[-1], exits[-1])

        stoch_.append(row.stoch)
        adx_.append(row.adx)
        macd_.append(row.macd)
        low.append(row.Low)
        high.append(row.High)
    total = profits + losses
    win_rate = (profits / total) * 100
    print(variation, pair, profits, losses, total, win_rate, sep=",", end="\n")
    return pair, list(zip(entries, exits)), [profits, losses, total, win_rate]


def handle_configs(configs, args):
    ohlc = pd.HDFStore(args.data, mode='r')
    print(f"Currency pairs : {ohlc.keys()}")
    print(f"Running variations: {configs}")
    results = {}
    keys = list(ohlc.keys())
    pairs = [_.split('/')[-1] for _ in keys]
    final_aggr = pd.DataFrame()
    variations = []
    for config_ in configs:
        config = yaml.safe_load(open(config_))
        variations.append(config['variation'])
        cur_results = {}
        for key, pair in zip(keys, pairs):
            pair, res, aggregated = backtest(ohlc.get(key), pair, config, args, variations[-1])
            aggregated.extend([pair, variations[-1]])
            final_aggr = final_aggr.append(aggregated)
            cur_results.update({pair: res})
        results.update({config['variation']: cur_results })
    final_aggr.to_csv(os.path.join(args.results, f"aggregated-trades_{variations[0]}-{variations[-1]}.csv"), index=False)
    pickle.dump(results, open(os.path.join(args.results, f"results_{variations[0]}-{variations[-1]}.pkl"), 'wb'))


def main():
    args = collect_args()
    configs = glob.glob(os.path.join(args.configs, '*.yaml'))
    configs = sorted(configs, key=lambda x: int(x.split('/')[-1].split('.')[0][1:]))
    chunk_size = ceil(len(configs) / args.process)
    config_chunks = [configs[x: x + chunk_size] for x in range(0, len(configs), chunk_size)]
    processess = [Process(target=handle_configs, args=(chunk, args,)) for chunk in config_chunks]
    [_.start() for _ in processess]
    [_.join() for _ in processess]


if __name__ == "__main__":
    main()
