#!/usr/bin/env python3
# coding: utf-8
import argparse
import os
import glob

import pandas as pd
import yaml

def collect_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str, default="configs.tsv", help="Config csv to generate yaml files")
    parser.add_argument("-t", "--to", type=str, default=".",
                        help="Location to save the yaml files in")
    args = parser.parse_args()
    return args


def main():
    args = collect_args()
    df = pd.read_csv(args.config, sep=",")
    cols = df.columns
    for var in df.itertuples():                           
        x = dict(zip(cols, var[1:]))                      
        print(x)                                          
        yaml.dump(x, open(os.path.join(args.to, f"{x['variation']}.yaml"), 'w+'))


if __name__ == "__main__":
    main()
