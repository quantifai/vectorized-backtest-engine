#!/usr/bin/env python3
import argparse
import itertools
import json
from multiprocessing import Pool
import pickle
import random

import numpy as np
import pandas as pd


def collect_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--ranges', '-r', type=str, default='ranges.json',
            help="Specify the ranges for the parameters")
    parser.add_argument('--process', '-p', type=int, default=8,
            help="Number of batches to run at a time")
    parser.add_argument('--model', '-m', type=str, default='model.pkl',
            help="Model to use for the variation predictions")
    parser.add_argument('--threshold', '-t', type=float, default=0.46,
            help="Minumum threshold to be logged to file")
    parser.add_argument('--checkpoint', '-c', type=int, default=1000)
    args = parser.parse_args()
    return args


args = collect_args()
ranges = json.load(open(args.ranges))
fields = "shift	timeout	sl	tp	slowperiod	fastperiod	signalperiod	macd_shift	histo_check	adx	adx_shift	stoch_fast	stoch_slow	stoch_long	stoch_short".split("\t")
permutations = np.array(list(itertools.product([0, 1], repeat=len(fields))))
model = pickle.load(open(args.model, 'rb'))

def generate(base, variations, fields, ranges):
    """Generate all possible up/down configs from base

    :param base: np.array -> base configuration (1 x 15)
    :param variations: np.array -> All variations run till now
    :param fields: list -> ordered list of fields
    :param ranges: dict -> start stop step values for all fields
    """

    new_valset = []
    for idx, field in enumerate(fields):
        if field == "tp":
            sls = new_valset[-1]
            new_valset.append([sls[0] * 1.5, sls[1] * 1.5])
            continue
        base_val = base[idx]
        loc_ = ranges[field].index(base_val)
        val_up = random.choice(ranges[field][loc_ + 1:]) if loc_ + 1 < len(ranges[field]) - 1 else  random.choice(ranges[field][:loc_])
        val_down = random.choice(ranges[field][:loc_]) if loc_ > 0 else  random.choice(ranges[field][loc_ + 1:])
        new_valset.append([val_down, val_up])
    new_valset = np.array(new_valset)
    configs = np.where(permutations, new_valset[:, 0], new_valset[:, 1])
    configs[:, fields.index("tp")] = configs[:, fields.index("sl")] * 1.5
    return configs


def permute_config(base_config, variations, final, threshold):
    configs = generate(base_config, variations, fields, ranges)
    valid = ~np.array([any(np.equal(variations, config).all(1)) for config in configs])
    configs = configs[valid].reshape(-1, len(fields))
    np.random.shuffle(configs)
    y_hat =  model.predict(configs)
    cur_final = np.hstack([configs, y_hat.reshape(-1, 1)])
    variations = np.vstack([variations, configs])
    bool_idxs = np.array(np.nonzero(y_hat >= threshold))
    if bool_idxs.size:
        print(f"Found {bool_idxs.size} > {threshold * 100}%")
        selected = configs[bool_idxs].squeeze()
        final_ = cur_final[bool_idxs].squeeze()
        final = np.vstack([final, final_])
        print(final[-10:, :])
        np.save(open('final_vars', 'wb'), final)
        new_threshold = threshold  # TODO: update threshold values every level
        for config in selected:
            permute_config(config, variations, final, new_threshold)


def main():
    print(f"Loaded model: {model}")
    base = np.array([2,3,6,9,20,15,10,1,1,16,0,4,12,30,70])
    variations = base.reshape(1, len(fields))
    final = np.hstack([base, model.predict([base])]) 
    configs = generate(base, variations, fields, ranges)
    np.random.shuffle(configs)
    for config in configs:
        permute_config(config, variations, final, args.threshold)


if __name__ == "__main__":
    main()
